<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin routes
//Route::get('admin', 'AdminController@create');
Route::post('authentication', 'AdminController@login');
Route::get('logout', 'AdminController@logout');
Route::post('update', 'AdminController@update');
Route::post('status', 'AdminController@status');
Route::get('admin', 'AdminController@index');
Route::get('dashboard', 'AdminController@dashboard');
Route::get('admin/paginate', 'AdminController@paginate');
Route::get('admin/filter', 'AdminController@filterByStatus');

// Home routes
Route::post('guest/create', 'HomeController@create');
Route::get('/', 'HomeController@index');
Route::get('guest/paginate', 'HomeController@paginate');
Route::get('guest/filter', 'HomeController@filterByStatus');
