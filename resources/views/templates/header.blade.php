<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Deday Tasks</title>
        <link rel="stylesheet" href="public/css/app.css">
        <link rel="stylesheet" href="public/css/source.css">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <body>
    <div class="container"><!--Container-->