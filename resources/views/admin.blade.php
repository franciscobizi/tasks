@include('templates.header')
   <header>
      <h1>Private Access</h1>
   </header>
   <main>
   <div class="row">
                <div class="col-sm-6 offset-3">
                    <div class="sidebar">
                        <form id="loginForm">
                           @csrf
                            <h3 class="text-center">Authentication</h3>
                            <hr>
                            <label>Username</label>
                            <input type="text" name="username" id="username" class="form-control" placeholder="Enter your username">
                            <label>Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password">
                            <button class="btn-submit">Login</button>
                        </form>
                    </div>
                    <br>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Message</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
   </main>
@include('templates.footer')

