@include('templates.header')
   <header>
      <h1>App for Creating Tasks</h1>
   </header>
   <main><!--Main block-->
      <div class="row">
         <div class="col-sm-12 text-center">
            <button href="#preview" title="Create new task" data-toggle="modal"><i class="fa fa-2x fa-plus-square" aria-hidden="true"> New Task</i></button>
         </div>
      </div>
       <br>
      <div class="row">
         <div class="col-sm-12" >
            <table class="table" id="sortTable">
               <thead class="thead-light">
                  <tr>
                     <th scope="col">Picture </th>
                     <th scope="col">Name  <span class="spins-down" id="nameDown" ></span> <span class="spins-up" data-down="name" id="nameUp"></span></th>
                     <th scope="col">Email <span class="spins-down" id="emailDown" ></span> <span class="spins-up" data-down="name" id="emailUp"></span></th>
                     <th scope="col">Description</th>
                     <th scope="col">Status <span class="spins-down" id="statusDown" ></span> <span class="spins-up" data-down="name" id="statusUp"></span></th>
                  </tr>
               </thead>
               <tbody id="container-table">
                  @foreach ($tasks as $task)
                     <tr>
                        <td><img src='public/images/tasks/{{$task->image}}' style='width: 100px;'/></td><td>{{$task->name}}</td><td>{{$task->email}}</td><td class='text-justify'>{{$task->description}}</td><td colspan='2'>{!!$task->status == 'checked' ? "Approved <br> <i class='fa fa-check' ></i>" : "Pending <br> <i class='fa fa-eye'></i>"
                     !!}</td> 
                     </tr>
                  @endforeach
               </tbody>
            </table>
            <div class="row">
               <div class="col-sm-6">

                  @if($total > 1)
                      <nav aria-label="Page navigation">
                      <ul class="pagination">
                           <li class="page-item"><a class="page-link paginate" data-url="guest" data-page="previous" title="Previous page"><i class="fa fa-backward activelink" aria-hidden="true"></i></a></li>
                           <li class="page-item"><a class="page-link">In <span id="curentPage">1</span> of <span id="totalPage">{{$total}}</span> pages</a></li>
                           <li class="page-item"><a class="page-link paginate" data-url="guest" data-page="next" title="Next page"><i class="fa fa-forward activelink" aria-hidden="true"></i></a></li>
                     </ul>
                      </nav>
                  @endif;
               </div>
               <div class="col-sm-6">
                  <select class="filter pull-right" data-url="guest">
                     <option value="0" readonly>Filter by status</option>
                     <option value="checked">Approved</option>
                     <option value="unchecked">Pending</option>
                  </select>
               </div>
            </div>
            
         </div>
      </div>
    </div>
</main><!--End Main block-->
@include('templates.footer')

