{{UserAuth::isLoggedIn()}}
@include('templates.header')
            <header>
               <h1>Dashboard</h1>
            </header>
            <main><!--Main block-->
                <div class="row">
                    <div class="col-sm-2">
                    <div class="sidebar">
                        <div class="text-center">
                           <img src='public/images/user.jpg' style='width: 100%;'/>
                           <h3>{{UserAuth::getUserName()}}</h3>
                           <hr>
                           <a href="logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                        </div>
                     </div>
                    </div>
                    <div class="col-sm-10">
                        <table class="table" id="sortTable">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Picture </th>
                                    <th scope="col">Name  <span class="spins-down" id="nameDown" ></span> <span class="spins-up" data-down="name" id="nameUp"></span></th>
                                    <th scope="col">Email <span class="spins-down" id="emailDown" ></span> <span class="spins-up" data-down="name" id="emailUp"></span></th>
                                    <th scope="col">Task</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="container-table-a">
                                @foreach($tasks as $task)
                                    <tr>
                                                <td><img src='public/images/tasks/{{$task->image}}' style='width: 100px;'/></td><td>{{$task->name}}</td><td class='text-justify'>{{$task->email}}</td><td class='text-justify'>{{$task->description}}</td><td><span data-toggle='modal' data-target='#ediTask' data-task='{{$task->description}}' data-id='{{$task->id}}'><i class='fa fa-edit' style='font-size: 1.5em;'></i></span>
                                                            <span class='checkbox'>
                                                              <label>
                                                                <input type='checkbox' class='check-status' data-id='{{$task->id}}' {{$task->status}}>
                                                                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
                                                              </label>
                                                            </span>
                                                        </td> 
                                            </tr>
                                 @endforeach
                            </tbody>
                        </table>
                        <form id="form-status" action="status" method="post">
                           @csrf
                            <input type="hidden" name="idstatus" id="idstatus">
                            <input type="hidden" name="status" id="status">
                        </form> 
                        <div class="row">
               <div class="col-sm-6">

                  @if($total > 1)
                      <nav aria-label="Page navigation">
                      <ul class="pagination">
                           <li class="page-item"><a class="page-link paginate" data-url="admin" data-page="previous" title="Previous page"><i class="fa fa-backward activelink" aria-hidden="true"></i></a></li>
                           <li class="page-item"><a class="page-link">In <span id="curentPage">1</span> of <span id="totalPage">{{$total}}</span> pages</a></li>
                           <li class="page-item"><a class="page-link paginate" data-url="admin" data-page="next" title="Next page"><i class="fa fa-forward activelink" aria-hidden="true"></i></a></li>
                     </ul>
                      </nav>
                  @endif;
               </div>
               <div class="col-sm-6">
                  <select class="filter pull-right" data-url="admin">
                     <option value="0" readonly>Filter by status</option>
                     <option value="checked">Approved</option>
                     <option value="unchecked">Pending</option>
                  </select>
               </div>
            </div>
         </div>
      </div>
   </div>
  </main><!--End main-->

@include('templates.footer')

