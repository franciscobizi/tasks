<?php

class UserAuth{

    /**
     * Verify if user is loggedin
     * @return void
     */
    public static function isLoggedIn(){
        if(!isset($_SESSION['admin']) && empty($_SESSION['admin'])){
            header("location: /admin");
            exit();
        }
    }

    /**
     * Get user data
     * @return void
     */
    public static function getUserName(){
        if(isset($_SESSION['admin']) && !empty($_SESSION['admin'])){
            echo ucfirst($_SESSION['admin']);
        }
    }

}
