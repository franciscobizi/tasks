<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Classes\Upload;
use App\Models\Task;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application index page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tasks = DB::table('tasks')
                ->offset(0)
                ->limit(3)
                ->get();

        $all = DB::table('tasks')->get();
        $total = count($all);
        $total_pages = ceil($total / 3);

        return view('welcome', ['tasks' => $tasks, 'total' => $total_pages]);
    }

    /**
     * Create new task.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'email'         => 'required|email',
            'task'          => 'required',
            'name'          => 'required',
            'image'         => 'required'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'There are empty fields'], 200);
        }

        $task = new Task();
        $task->name = $request->input('name');
        $task->email = $request->input('email');
        $task->description = $request->input('task');
        $task->status = 'Unchecked';

        if(isset($_FILES['image']) && $_FILES['image']['size'] != null){
            $upload = new Upload($_FILES['image'], 320, 240, dirname(__DIR__)."/../../public/images/tasks/");
            $path = $upload->save();
            if (!$path) {
                return response()->json(['status' => 400, 'message' => 'Extentions allowed for image are [jpg, gif, png]!'], 200);
            }
            $task->image = basename($path);
            $task->save();
        }

        return response()->json(['status' => 200, 'message' => 'Task added successfully!'], 200);
    }

    /**
    *   
    *  Method for paginate items on the page
    *
    *  @return array $tasks
    */
    public function paginate(Request $request)
    {
        if($request->has('page')){
            $page = (int)$request->get('page');
            $skip = ($page - 1) * 3;
            $tasks = DB::table('tasks')->skip($skip)->take(3)->get();
            if(count($tasks) > 0){
                return response()->json(['status' => 200, 'tasks' => $tasks], 200);
            }else{
                return response()->json(['status' => 400, 'message' => 'No more data to fetch'], 200);
            }
        }
    }

    /**
    *   
    *  Method for paginate items on the page
    *
    *  @return array $tasks
    */
    public function filterByStatus(Request $request)
    {
        if($request->has('status')){
            $status = $request->get('status');
            $tasks = DB::table('tasks')->where('status', $status)->get();
            if(count($tasks) > 0){
                return response()->json(['status' => 200, 'tasks' => $tasks], 200);
            }else{
                return response()->json(['status' => 400, 'message' => 'No more data to fetch'], 200);
            }
        }
    }
}
