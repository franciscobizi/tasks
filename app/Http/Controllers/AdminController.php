<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class AdminController extends Controller
{
    /**
     * Method for displaying auth form
     * 
     * @return void
     */
    public function index(){
        return view('admin'); 
    }

    /**
     * Method for authentication
     * 
     * @return void
     */
    public function login(Request $request){

        $validator = Validator::make(request()->all(), [
            'username'         => 'required',
            'password'      => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'Empty fields or invalid values!'], 200);
        }

        $user = DB::table('users')->where('username', '=', $request->input('username'))->get();
         
        if(count($user) > 0 && password_verify($request->input('password'), $user[0]->password)){
            self::auth($user[0]->username);
            return response()->json(['status' => 200, 'message' => 'You are logged!'], 200);
        }
        return response()->json(['status' => 401, 'message' => 'Incorrect username or password!'], 200);   
    }

    /**
     * Logout user from app
     * @return void
     */
    public function logout(){
        self::isLogout();
    }


    /**
     * Show dashboard page with the tasks
     * @return void
     */
    public function dashboard(){
        $tasks = DB::table('tasks')
                ->offset(0)
                ->limit(3)
                ->get();

        $all = DB::table('tasks')->get();
        $total = count($all);
        $total_pages = ceil($total / 3);

        return view('dashboard', ['tasks' => $tasks, 'total' => $total_pages]);
    }

    // just for test propose
    public function create(){

       /*  $has_pass =  password_hash('123', PASSWORD_BCRYPT, ['cost' => 12]);
        DB::table('users')->insert(
            ['username' => 'admin', 'password' => $has_pass]
        );
        return 'Created user admin'; */
    }

    /**
    *   
    *  Method for editing tasks
    *
    *  @return void
    */
	public function update(Request $request)
	{
        $validator = Validator::make(request()->all(), [
            'task'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'Empty fields or invalid values!'], 200);
        }

        DB::table('tasks')
            ->where('id', $request->input('taskid'))
            ->update(['description' => $request->input('task')]);

            return response()->json(['status' => 200, 'message' => 'Updated successful!'], 200);
    }
    
    /**
    *   
    *  Method for changing tasks status
    *
    *  @return void
    */
    public function status(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'idstatus' => 'required',
            'status'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'Empty fields or invalid values!'], 200);
        }

        DB::table('tasks')
            ->where('id', $request->input('idstatus'))
            ->update(['status' => $request->input('status')]);

        header('location: /dashboard');
        exit();

    }


    /**
    *   
    *  Method for paginate items on the page
    *
    *  @return void
    */
    public function paginate(Request $request)
    {
        if($request->has('page')){
            $page = (int)$request->get('page');
            $skip = ($page - 1) * 3;
            $tasks = DB::table('tasks')->skip($skip)->take(3)->get();
            if(count($tasks) > 0){
                return response()->json(['status' => 200, 'tasks' => $tasks], 200);
            }else{
                return response()->json(['status' => 400, 'message' => 'No more data to fetch'], 200);
            }
        }
    }

    /**
    *   
    *  Method for paginate items on the page
    *
    *  @return void
    */
    public function filterByStatus(Request $request)
    {
        if($request->has('status')){
            $status = $request->get('status');
            $tasks = DB::table('tasks')->where('status', $status)->get();
            if(count($tasks) > 0){
                return response()->json(['status' => 200, 'tasks' => $tasks], 200);
            }else{
                return response()->json(['status' => 400, 'message' => 'No more data to fetch'], 200);
            }
        }
    }

    /**
     * Set session admin
     * @param string $name
     * @return void
     */
    private static function auth($name){
        $_SESSION['admin'] = $name;
    }

     /**
     * Logout user from app
     * @return void
     */
    private static function isLogout(){
        unset($_SESSION['admin']);
        header("location: /admin");
        exit();
    }
    
}
