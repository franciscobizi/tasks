$(document).ready(function() {

  // Authentication
  $( "#loginForm" ).on('submit',function(e) {
    e.preventDefault();
    /* var username = $('#username').val(); 
    var password = $('#password').val(); */

    $.ajax({
           url : 'authentication',
           type: "POST",
           async: true,
           dataType:'json',
           data:  new FormData(this),
           contentType: false,
                 cache: false,
           processData:false,
      success : function(resp){
          if (resp.status == 200) {
              window.location.href = 'dashboard'; 
          }else{
              $('.alert STRONG').text(resp.message);
              $('.alert').show();
          }
      },
      error : function(resp){
         console.log(resp);
      }

    });
  });

  // Updating tasks
  $( "#ediTheTask" ).on('submit',function(e) {
    e.preventDefault();
    $.ajax({
      url : "update",
      type: "POST",
      async: true,
      dataType:'json',
      data:  new FormData(this),
      contentType: false,
            cache: false,
      processData:false,
      success : function(resp){
          if (resp.status == 200) {
              $('.alert').addClass('alert-success');
              $('.alert STRONG').text(resp.message);
              $('.alert').show(); 
          }else{
              $('.alert').addClass('alert-danger');
              $('.alert STRONG').text(resp.message);
              $('.alert').show();
          }
      },
      error : function(resp){
         console.log(resp);
      }

    });
  });

  // Creating tasks
  $( "#addingTheTask" ).on('submit',function(e) {
        e.preventDefault();

        $.ajax({
           url: 'guest/create',
           type: "POST",
           async: true,
           dataType:'json',
           data:  new FormData(this),
           contentType: false,
                 cache: false,
           processData:false,
           success : function(resp){
                if (resp.status == 200) {
                    $('.alert').addClass('alert-success');
                    $('.alert strong').text(resp.message);
                    $('.alert').show();

                }else{
                    $('.alert').addClass('alert-danger');
                    $('.alert strong').text(resp.message);
                    $('.alert').show();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
  });

  // Changing task's status
  $('body').on('change','.check-status',function(){
        if(this.checked){
        	$('#idstatus').val($(this).data('id'));
        	$('#status').val('checked');
        	$("#form-status").submit();
        }else{
          $('#idstatus').val($(this).data('id'));
        	$('#status').val('unchecked');
        	$( "#form-status" ).submit();
        }
  });

  // Trigger of filter tasks by status
	$(".filter").on('change',function(e) {
    e.preventDefault();
		var status = $(this).val();
    var url   = $(this).data('url');
    if(status != '0'){
      filterByStatus(status, url);
    }
	});

  // Trigger of pagination tasks
	$( ".paginate" ).click(function() {

		var current = $('#curentPage').text();
		var total   = $('#totalPage').text();
		var _this   = $(this).data('page');
    var url   = $(this).data('url');

		if (_this == 'previous') {
			current = parseInt(current);
			if (current > 1) {
  				current--;
  				$('#curentPage').text(current);
  				$('#page_n').val(current);
          paginate(current, url);
			}
		}else{
			if (current < parseInt(total)) {
  				current++;
  				$('#curentPage').text(current);
  				$('#page_n').val(current);
          paginate(current, url);
			}
		}
		
	});

  // Show the preview body
	$('.showPreview').on('click', function (event) {
          $('#add-body').hide();
          $('#exampleModalCenterTitle').text('Preview the task');
          $('#preview-body').show();
          var button = $(event.relatedTarget);
          var name   = $('#name').val();
          var email  = $('#email').val();
          var task   = $('#task').val();
          var modal  = $('#preview');
          modal.find('#pname').text(name);
          modal.find('#pemail').text(email);
          modal.find('#ptask').text(task);
  });

  // Hidde the preview body
  $('#backAddTask').on('click', function (event) {
          $('#preview-body').hide();
          $('#exampleModalCenterTitle').text('Adding new task');
          $('#add-body').show();
  });

  // Display modal for editing tasks
  $('#ediTask').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var task   = button.data('task');
          var id     = button.data('id');
          var modal  = $(this);
          modal.find('#etask').val(task);
          modal.find('#taskid').val(id);
  });

  // Hidding alerts messages  
	$('.close').click(function(){
		 $('.alert').hide();
     window.location.reload();
	});

  // Sorting taks by keys
	$('#nameUp, #nameDown, #emailUp, #emailDown, #statusUp, #statusDown').each(function(){
	        var table = $('#sortTable');  
	 		var th = $(this),
                thIndex = th.index(),
                inverse = false;

            th.click(function(){
                
                table.find('td').filter(function(){
                    
                    return $(this).index() === thIndex;
                    
                }).sortElements(function(a, b){
                    
                    return $.text([a]) > $.text([b]) ?
                        inverse ? -1 : 1
                        : inverse ? 1 : -1;
                    
                }, function(){
                    return this.parentNode; 
                    
                });
                
                inverse = !inverse;
                    
            });          
	});
	
});


function loadFile(event) {
	var preview = document.getElementById('imagePreview');
    preview.src = URL.createObjectURL(event.target.files[0]);
};

/**
* function for paginate the tasks
* @param int var current
* @param string var url
* @return array tasks 
*/
function paginate(current, url){
    $.ajax({
          method : 'get',
             url : url + '/paginate',
        dataType : 'json',
            data : {page : current},
        success : function(resp){
            var data = resp['tasks'];
            var table = $("#container-table");
            var table_a = $("#container-table-a");
            table.html('');
            table_a.html('');
            if (resp.status == 200) {
                 if (url == 'admin') {
                    for (var i = 0; i < data.length; i++) {
                       var task = data[i];
                       status = task.status == 'checked' ? 'Approved <br> <i class="fa fa-check" aria-hidden="true"></i>' : 'Pending <br> <i class="fa fa-eye" aria-hidden="true"></i>';
                       table_a.append(
                          "<tr><td><img src='public/images/tasks/"+task.image+"' style='width: 100px;'/></td><td>"+task.name+"</td><td>"+task.email+"</td><td class='text-justify'>"+
                          task.description+"</td><td><span data-toggle='modal' data-target='#ediTask' data-task='"+task.description+"' data-id='"+task.id+"'><i class='fa fa-edit' style='font-size: 1.5em;'></i></span>"+
                                                            "<span class='checkbox'>"+
                                                              "<label>"+
                                                                "<input type='checkbox' class='check-status' data-id='"+task.id+"' "+task.status+">"+
                                                                "<span class='cr'><i class='cr-icon fa fa-check'></i></span>"+
                                                              "</label></span></td> </tr>"
                       );
                   }
                 }else{
                   for (var i = 0; i < data.length; i++) {
                       var task = data[i];
                       status = task.status == 'checked' ? 'Approved <br> <i class="fa fa-check" aria-hidden="true"></i>' : 'Pending <br> <i class="fa fa-eye" aria-hidden="true"></i>';
                       table.append(
                          "<tr><td><img src='public/images/tasks/"+task.image+"' style='width: 100px;'/></td><td>"+task.name+"</td><td>"+task.email+"</td><td class='text-justify'>"+
                          task.description+"</td><td colspan='2'>"+status+"</td> </tr>"
                       );
                   }
                 }
            }
        },
        error : function(resp){
           console.log(resp);
        }

      });
}


/**
* Function for filtering the tasks by status
* @param string var status
* @param string var url
* @return array tasks 
*/
function filterByStatus(status, url){
  $.ajax({
        method : 'get',
           url : url + '/filter',
      dataType : 'json',
          data : {status : status},
      success : function(resp){
          var data = resp['tasks'];
          var table = $("#container-table");
          var table_a = $("#container-table-a");
          table.html('');
          table_a.html('');
          if (resp.status == 200) {
               if (url == 'admin') {
                  for (var i = 0; i < data.length; i++) {
                     var task = data[i];
                     status = task.status == 'checked' ? 'Approved <br> <i class="fa fa-check" aria-hidden="true"></i>' : 'Pending <br> <i class="fa fa-eye" aria-hidden="true"></i>';
                     table_a.append(
                        "<tr><td><img src='public/images/tasks/"+task.image+"' style='width: 100px;'/></td><td>"+task.name+"</td><td>"+task.email+"</td><td class='text-justify'>"+
                        task.description+"</td><td><span data-toggle='modal' data-target='#ediTask' data-task='"+task.description+"' data-id='"+task.id+"'><i class='fa fa-edit' style='font-size: 1.5em;'></i></span>"+
                                                          "<span class='checkbox'>"+
                                                            "<label>"+
                                                              "<input type='checkbox' class='check-status' data-id='"+task.id+"' "+task.status+">"+
                                                              "<span class='cr'><i class='cr-icon fa fa-check'></i></span>"+
                                                            "</label></span></td> </tr>"
                     );
                 }
               }else{
                 for (var i = 0; i < data.length; i++) {
                     var task = data[i];
                     status = task.status == 'checked' ? 'Approved <br> <i class="fa fa-check" aria-hidden="true"></i>' : 'Pending <br> <i class="fa fa-eye" aria-hidden="true"></i>';
                     table.append(
                        "<tr><td><img src='public/images/tasks/"+task.image+"' style='width: 100px;'/></td><td>"+task.name+"</td><td>"+task.email+"</td><td class='text-justify'>"+
                        task.description+"</td><td colspan='2'>"+status+"</td> </tr>"
                     );
                 }
               }
          }
      },
      error : function(resp){
         console.log(resp);
      }

    });
}

